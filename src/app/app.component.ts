import {MediaMatcher} from '@angular/cdk/layout';
import { Component, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-root',
  template:`
  <div>
    <app-layout></app-layout>
  </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'cEats';


  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}