import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareduiModule } from '../shared/sharedui/sharedui.module';
import { RouterModule } from '@angular/router';
import { OrderComponent } from './order.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'order', component: OrderComponent }
    ]),
    ShareduiModule,
  ],
  declarations: [OrderComponent]
})
export class OrderModule { }
