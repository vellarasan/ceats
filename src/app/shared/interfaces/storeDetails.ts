export interface StoreDetailsValue extends Coordinates {
    storeName:string,
    description:string,
    coordinates?:Coordinates
}